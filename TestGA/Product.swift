//
//  Product.swift
//  TestGA
//
//  Created by Thanya on 28/5/20.
//  Copyright © 2020 ForThan. All rights reserved.
//

import UIKit

struct Product: Codable {
    let name: String
    let price: Int
}

struct Transaction {
    let product: Product
    let transactionId: String
    let tax: Int
    let shipping: Int
    let currencyCode: String
    let affiliation: String
}
