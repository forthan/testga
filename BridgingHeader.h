//
//  BridgingHeader.h
//  TestGA
//
//  Created by Thanya on 27/5/20.
//  Copyright © 2020 ForThan. All rights reserved.
//

#ifndef BridgingHeader_h
#define BridgingHeader_h

#import <GoogleAnalytics/GAI.h>
#import <GoogleAnalytics/GAIDictionaryBuilder.h>
#import <GoogleAnalytics/GAIFields.h>
#endif /* BridgingHeader_h */
