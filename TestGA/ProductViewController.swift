//
//  ProductViewController.swift
//  TestGA
//
//  Created by Thanya on 27/5/20.
//  Copyright © 2020 ForThan. All rights reserved.
//

import UIKit
import FirebaseAnalytics
class ProductViewController: UIViewController {
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    var product: String!
    var price: String!
    override func viewDidLoad() {
        super.viewDidLoad()
        productName.text = product
        priceLabel.text = price
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        GA().send(GAAction.Screen(view: .ProductViewController))
    }
    
    @IBAction func buy(_ sender: Any) {
        GA().send(.Buy(transaction: Transaction(product: Product(name: product, price: Int(price ?? "0") ?? 0), transactionId: "\(Int.random(in: 100000...999999))", tax: 7, shipping: 10, currencyCode: "USD", affiliation: "In-app Store")))
        
        let alert = UIAlertController(title: "Buy Success", message: "", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            self.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true)
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
