//
//  ViewController.swift
//  TestGA
//
//  Created by Thanya on 27/5/20.
//  Copyright © 2020 ForThan. All rights reserved.
//

import UIKit
import FirebaseAnalytics
class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
            AnalyticsParameterItemID: "id-\("ViewController")",
            AnalyticsParameterItemName: "ViewController",
            AnalyticsParameterContentType: "image"
        ])
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        GA().send(GAAction.Screen(view: .ViewController))
    }
    
    @IBAction func sentEvent() {
        Analytics.logEvent("event", parameters: [
            "image_name": "name",
            "full_text": "text"
        ])
        
    }
    
}

extension ViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 20
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ProductCollectionViewCell
        cell.productName.text = "product \(indexPath.item + 1)"
        cell.priceLabel.text = "\((indexPath.item + 1) * 10)"
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as? ProductCollectionViewCell
        GA().send(.Select(item: Product(name: cell?.productName.text ?? "", price: Int(cell?.priceLabel.text ?? "0") ?? 0)))
        guard let productViewController = self.storyboard?.instantiateViewController(identifier: "ProductViewController") as? ProductViewController else {
            return
        }
    
        productViewController.product = cell?.productName.text
        productViewController.price = cell?.priceLabel.text
        self.show(productViewController, sender: nil)
    }
    
}

