//
//  GA.swift
//  TestGA
//
//  Created by Thanya on 28/5/20.
//  Copyright © 2020 ForThan. All rights reserved.
//

import Foundation
import FirebaseAnalytics
struct GA {
    private let tracker = GAI.sharedInstance().defaultTracker
}

extension GA {
    
    func send(_ action: GAAction) {
        switch action {
        case .Screen(let view):
            tracker?.set(kGAIScreenName, value: view.rawValue)
            guard let builder = GAIDictionaryBuilder.createScreenView() else { return }
            tracker?.send(builder.build() as [NSObject : AnyObject])
        case .Select(let product):
            tracker?.set(kGAIProduct, value: product.name)
            guard let builder = GAIDictionaryBuilder.createEvent(withCategory: "select_product", action: "select", label:  product.name, value: 1 as NSNumber) else { return }
            tracker?.send(builder.build() as [NSObject : AnyObject])
            
        case .Buy(let transaction):
            guard  let buyBuilder = GAIDictionaryBuilder.createTransaction(withId: transaction.transactionId, affiliation:transaction.affiliation, revenue: transaction.product.price as NSNumber , tax: transaction.tax as NSNumber, shipping: transaction.shipping as NSNumber , currencyCode: transaction.currencyCode) else {
                return
            }
            tracker?.send(buyBuilder.build() as [NSObject : AnyObject])
        }
    }
    
}

enum GAAction {
    case Screen(view: ScreenName)
    case Select(item: Product)
    case Buy(transaction: Transaction)
    
}

enum ScreenName: String {
    case ViewController = "ViewController"
    case ProductViewController = "ProductViewController"
}
