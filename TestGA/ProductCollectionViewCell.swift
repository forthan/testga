//
//  ProductCollectionViewCell.swift
//  TestGA
//
//  Created by Thanya on 27/5/20.
//  Copyright © 2020 ForThan. All rights reserved.
//

import UIKit
import FirebaseAnalytics
class ProductCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBAction func buy(_ sender: Any) {
        GA().send(.Buy(transaction: Transaction(product: Product(name: productName.text ?? "", price: Int(priceLabel.text ?? "0") ?? 0), transactionId: "\(Int.random(in: 100000...999999))", tax: 7, shipping: 10, currencyCode: "USD", affiliation: "In-app Store")))
        let alert = UIAlertController(title: "Buy Success", message: "", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            
        }))
        self.parentViewController?.present(alert, animated: true)
    }
    
}
extension UIView {
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if parentResponder is UIViewController {
                return parentResponder as? UIViewController
            }
        }
        return nil
    }
}
