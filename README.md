**Build and Runtime Requirements**

Xcode Version 11.5 or later

### How to run the example?

1. Clone this repo
1. Open shell window and navigate to project folder
1. Run `pod install`
1. Open `TestGA.xcworkspace` and run the project on selected device or simulator